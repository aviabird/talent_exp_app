"use strict"

# Prod2 config
window.CONFIG =
  url: ""
  apiUrl: "/api"
  apiTalentUrl: '/api/recruiter'
  raven_dev_status: false
  myveeta_verification_token: ''
  intercomAppId: ''
