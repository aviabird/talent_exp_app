"use strict"

# local settings
window.CONFIG =
  url: "http://localhost:3000"
  apiUrl: "http://localhost:3000/api"
  apiTalentUrl: 'http://localhost:3000/api/recruiter'
  ApiMockUrl: ''
  ApiTalentMockUrl: ''
  ApiJsonUrl: 'http://localhost:9000/api_mocks'
  raven_dev_status: true
  myveeta_verification_token: ''
  intercomAppId: ''
