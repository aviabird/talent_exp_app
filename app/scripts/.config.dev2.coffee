"use strict"

# Dev2 config
window.CONFIG =
  url: "https://api-explorer-dev2.t13s.at"
  apiUrl: "https://api-explorer.t13s.at/api"
  apiTalentUrl: 'https://api-explorer.t13s.at/api/talent'
  raven_dev_status: false
  myveeta_verification_token: ''
  intercomAppId: ''
