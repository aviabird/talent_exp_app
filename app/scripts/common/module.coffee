###*
  # @ngdoc overview
  # @name common
  # @description holds the common utils
###

angular.module 'myVeetaTalentExplorerApp.common', [
  # System Core
  'ngAnimate'
  'ngCookies'
  'ngMessages'
  'ngResource'
  'ngSanitize'
  'ngTouch'
  
  # Routing
  'ui.router'
  'ui.bootstrap'
  
  # Auth
  'ng-token-auth'

  # Add more system wide dependencies here

  # Modules
  'myVeetaTalentExplorerApp.common.controllers'
  'myVeetaTalentExplorerApp.common.directives'
  'myVeetaTalentExplorerApp.common.services'
  'myVeetaTalentExplorerApp.common.resources'
  'myVeetaTalentExplorerApp.common.filters'
  
]

angular.module 'myVeetaTalentExplorerApp.common.controllers', []
angular.module 'myVeetaTalentExplorerApp.common.directives', []
angular.module 'myVeetaTalentExplorerApp.common.services', []
angular.module 'myVeetaTalentExplorerApp.common.resources', []
angular.module 'myVeetaTalentExplorerApp.common.filters', []