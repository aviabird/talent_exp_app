angular.module("myVeetaTalentExplorerApp")
.config ($stateProvider
         $urlRouterProvider
         LAYOUTS
         DASHBOARD
         ) ->

  $urlRouterProvider.otherwise("/dashboard");

  $stateProvider
  # base layout
  .state "app",
    abstract: true
    templateUrl: LAYOUTS.VIEWS.APPLICATION
    controller: "AppCtrl"

  .state "app.dashboard",
  # dashboard page - don't forget to put it behind sign in
    url: "/dashboard",
    templateUrl: DASHBOARD.VIEWS.OVERVIEW
    controller: 'DashboardOverviewCtrl'
