angular.module 'myVeetaTalentExplorerApp'
.constant 'LAYOUTS',
  VIEWS:
    APPLICATION: 'views/layouts/application.html'
  
  PARTIALS:
    HEADER: 'views/layouts/header.html'
    FOOTER: 'views/layouts/footer.html'

.constant 'DASHBOARD',
  VIEWS:
    OVERVIEW: 'views/pages/dashboard/overview.html'
