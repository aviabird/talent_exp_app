'use strict'

###*
  # @name common.controller.AppCtrl
  #
  # @requires $scope
  # @requires $rootScope
###

angular.module("myVeetaTalentExplorerApp.common.controllers")
.controller "AppCtrl", ($scope, $rootScope) ->
  console.log "In app ctrl"