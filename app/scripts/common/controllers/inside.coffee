'use strict'

###*
  # @name common.controller.InsideCtrl
  #
  # @requires $scope
  # @requires $rootScope
###

angular.module("myVeetaTalentExplorerApp.common.controllers")
.controller "InsideCtrl", ($scope, $rootScope) ->
  console.log "In inside ctrl"