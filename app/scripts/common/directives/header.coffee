"use strict"

###*
  # @ngdoc directive
  # @name 
  #
###

angular.module('myVeetaTalentExplorerApp.common.directives')
.directive 'appHeader', ($rootScope, $state, LAYOUTS) ->
  restrict: 'AE'
  templateUrl: LAYOUTS.PARTIALS.HEADER
  replace: true
  scope: {}
  controller: ($scope) ->
    # In header directive controller
    console.log "In header controller"

    $scope.isShowUserNav = ->
      true