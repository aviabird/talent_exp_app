"use strict"

###*
  # @ngdoc directive
  # @name 
  #
###

angular.module('myVeetaTalentExplorerApp.common.directives')
.directive 'footer', ($rootScope, $state, LAYOUTS) ->
  restrict: 'AE'
  templateUrl: LAYOUTS.PARTIALS.FOOTER
  replace: true
  scope: {}
  controller: ($scope) ->
    # In header directive controller
    console.log "In footer controller"