'use strict'

###*
 # @ngdoc function
 # @name myVeetaTalentExplorerApp.controller:DashboardOverviewCtrl
 # @description
 # # DashboardOverviewCtrl
 # Controller of the myVeetaTalentExplorerApp
###
angular.module 'myVeetaTalentExplorerApp.controllers'
.controller 'DashboardOverviewCtrl', ->
  console.log "In dashboard overview ctrl"
