'use strict'

###*
  # @name dashboard
  #
###

angular.module "myVeetaTalentExplorerApp.dashboard", [
  "myVeetaTalentExplorerApp.dashboard.controllers",
  "myVeetaTalentExplorerApp.dashboard.directives",
  "myVeetaTalentExplorerApp.dashboard.resources"
]

angular.module 'myVeetaTalentExplorerApp.dashboard.controllers', []
angular.module 'myVeetaTalentExplorerApp.dashboard.directives', []
angular.module 'myVeetaTalentExplorerApp.dashboard.resources', []