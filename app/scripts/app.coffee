'use strict'

###*
 # @ngdoc overview
 # @name myVeetaTalentExplorerApp
 # @description
 # # myVeetaTalentExplorerApp
 #
 # Main module of the application.
###
angular
  .module 'myVeetaTalentExplorerApp', [
    
    # Include all dependencies in common module
    'myVeetaTalentExplorerApp.common'

    # common module

    # components
    'myVeetaTalentExplorerApp.dashboard'

    # modules ctrls, services
    'myVeetaTalentExplorerApp.controllers'
    'myVeetaTalentExplorerApp.directives'
    'myVeetaTalentExplorerApp.resources'

    # 
    
  ]

angular.module 'myVeetaTalentExplorerApp.controllers', []
angular.module 'myVeetaTalentExplorerApp.directives', []
angular.module 'myVeetaTalentExplorerApp.resources', []
