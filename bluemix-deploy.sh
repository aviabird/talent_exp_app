#!/bin/bash

# connect to bluemix and deploy

# env dependent settings
sentry_url_dev2='https:\/\/56605042457f41a7a33b2b121a08cf4f@app.getsentry.com\/47846'
sentry_url_prod2='https:\/\/36a7e19b6b8f4e4cad4c64694dde1fed@app.getsentry.com\/50135'
ga_code_dev2="UA-59279083-1"
ga_code_prod2="UA-59279083-4"
var="ga_code_${1}"
sentry_url_var="sentry_url_${1}"

#build app
cp app/scripts/.config.${1}.coffee app/scripts/config.coffee
grunt build
cp manifest-${1}.yml dist/.
cp Staticfile dist/.
if [ ${1} != 'prod2' ];
  then cp staging-protection.txt dist/Staticfile.auth
fi
#start deployment
cf login -o 'Talent Solutions' -s ${1}

#replace google analytics placeholder
#echo "Searching for |GA_CODE| in index.html..."
#grep -n "|GA_CODE|" dist/index.html
#echo "Setting google analytics code to ${!var}"
#sed -i "" "s/|GA_CODE|/${!var}/g" dist/index.html

#replace raven/sentry placeholder
#echo "Searching for |SENTRY_URL| in index.html..."
#grep -n "|SENTRY_URL|" dist/index.html
#echo "Setting sentry url to ${!sentry_url_var}"
#sed -i "" "s/|SENTRY_URL|/${!sentry_url_var}/g" dist/index.html

#deploy
#cf zero-downtime-push vta-app-prod2 -f dist/${1}-manifest.yml
./app-deploy-with-rename.sh r-${1} r-${1}-bak . dist/manifest-${1}.yml
#sed -i "" "s/${!var}/|GA_CODE|/g" dist/index.html
