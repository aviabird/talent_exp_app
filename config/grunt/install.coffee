module.exports = (grunt, appConfig) -> tasks:
  # Automatically inject Bower components into the app
  wiredep:
    app:
      src: ["#{appConfig.paths.app}/index.jade"]
      ignorePath: /\.\.\//

    sass:
      src: ["#{appConfig.paths.app}/styles/{,*/}*.{scss,sass}"]
      ignorePath: /(\.\.\/){1,2}bower_components\//


    karma:
      src: ["#{appConfig.paths.test}/karma-dev.conf.coffee"]
      ignorePath: /\.\.\//
      devDependencies: true
      fileTypes:
        coffee:
          block: /(([ \t]*)\#\s*bower:*(\S*))(\n|\r|.)*?(\#\s*endbower)/gi
          detect: {}
          replace:
            js: '\'{{filePath}}\''

  # Automatically inject scripts and styles into the app
  injector:
    app:
      options:
        starttag: "// injector:js"
        endtag: "// endinjector"
        ignorePath: "app/"
        addRootSlash: false
        transform: (filepath) ->
          e = require("path").extname(filepath).slice(1)
          if (e is 'coffee')
            filepath = filepath.replace /.coffee/, ".js"
          'script(src="' + filepath + '")'
      files:
        "<%= paths.app %>/index.jade": appConfig.paths.scripts

    karma:
      options:
        starttag: "# injector"
        endtag: "# endinjector"
#        ignorePath: "ngapp/<%= paths.appName %>/"
        addRootSlash: false
        transform: (filepath) ->
          e = require("path").extname(filepath).slice(1)
          return "'" + filepath + "'"  if e is "js" or e is "coffee"
      files:
        "<%= paths.test %>/karma-dev.conf.coffee": appConfig.paths.scripts
