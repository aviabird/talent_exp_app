module.exports = (grunt, appConfig) -> tasks:

  replace:
    server:
      options:
        patterns: [
          match: 'BASE_URL'
          replacement: '/'
        ]
      files: [
        {
          expand: true
          flatten: true
          src: ["#{appConfig.paths.tmp}/index.html"]
          dest: "#{appConfig.paths.tmp}/"
        }
      ]
    dist:
      options:
        patterns: [
          match: 'BASE_URL'
          replacement: '/'
        ]
      files: [
        {
          expand: true
          flatten: true
          src: ["#{appConfig.paths.dist}/index.html"]
          dest: "#{appConfig.paths.dist}/"
        }
      ]
