module.exports = (grunt, appConfig) -> tasks:
  watch:
    livereload:
      options:
        livereload: "<%= connect.options.livereload %>"
        spawn: false

      files: [
        "#{appConfig.paths.app}/{,*/}*.html"
        "#{appConfig.paths.tmp}/styles/{,*/}{,*/}{,*/}*.css"
        "#{appConfig.paths.tmp}/scripts/**/*.js"
        "#{appConfig.paths.tmp}/views/**/*.html"
        "#{appConfig.paths.app}/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}"
      ]
    css:
      options:
        livereload: "<%= connect.options.livereload %>"
        spawn: false
      files: [
        "#{appConfig.paths.app}/styles/{,*/}{,*/}{,*/}*.scss"
      #  "#{appConfig.paths.tmp}/styles/{,*/}{,*/}{,*/}*.scss"
      ]
      tasks: ['compass:dist']


  # The actual grunt server settings
  connect:
    options:
      port: 9000
    # Change this to '0.0.0.0' to access the server from outside.
      hostname: "localhost"
      livereload: 35729

    livereload:
      options:
        open: true
        middleware: (connect) ->
          [
            connect.static(".tmp")
            connect().use("/bower_components", connect.static("./bower_components"))
            connect.static(appConfig.paths.app)
            connect.static('api_mocks')
          ]

    test:
      options:
        port: 9001
        middleware: (connect) ->
          [
            connect.static(".tmp")
            connect.static("test")
            connect().use("/bower_components", connect.static("./bower_components"))
            connect.static(appConfig.paths.app)
          ]

    dist:
      options:
        open: true
        base: "#{appConfig.paths.dist}"

  # Run some tasks in parallel to speed up the build process
  concurrent:
    server: [
      "coffee:dist"
      "jade"
      "compass:server"
    ]
    test: [
      "coffee"
      "jade"
      "compass:server"
    ]
    dist: [
      "coffee"
      "jade"
      "compass:dist"
      "imagemin"
      "svgmin"
    ]
