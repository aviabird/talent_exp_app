# Generated on 2014-10-28 using generator-angular 0.9.8
"use strict"

# # Globbing
# for performance reasons we're only matching one level down:
# 'test/spec/{,*/}*.js'
# use this if you want to recursively match all subfolders:
# 'test/spec/**/*.js'
module.exports = (grunt) ->

  # Load grunt tasks automatically
  require("load-grunt-tasks") grunt

  # Time how long tasks take. Can help when optimizing build times
  require("time-grunt") grunt

  # Configurable paths for the application
  appConfig =
    config:
      src: "config/grunt/*"

    paths:
      app: require("./bower.json").appPath or "app"
      dist: "dist"
      test: "test"
      tmp: ".tmp"

      scripts: [
        'app/scripts/app.coffee'
        'app/scripts/common/config/**/*.coffee'

        # Common code
        'app/scripts/common/module.coffee'
        'app/scripts/common/{,*/}{,*/}*.coffee'

        # Components
        'app/scripts/components/dashboard/module.coffee'
        'app/scripts/components/dashboard/**/*.coffee'

      ]

  # Define the configuration for all the tasks
  configs = require("load-grunt-configs") grunt, appConfig

  grunt.initConfig configs


  grunt.registerTask "serve", "Compile then start a connect web server", (target) ->
    if target is "dist"
      return grunt.task.run([
        "build"
        "connect:dist:keepalive"
      ])
    grunt.task.run [
      "clean:server"
      "install"
      "concurrent:server"
      "replace:server"
      "autoprefixer"
      "connect:livereload"
      "watch"
    ]

  grunt.registerTask "server", "DEPRECATED TASK. Use the \"serve\" task instead", (target) ->
    grunt.log.warn "The `server` task has been deprecated. Use `grunt serve` to start a server."
    grunt.task.run ["serve:" + target]

  grunt.registerTask "test", [
    "clean:server"
    "concurrent:test"
    "autoprefixer"
    "connect:test"
    "karma"
  ]
  grunt.registerTask "build", [
    "clean:dist"
    "install"
    "jade"
    "useminPrepare"
    "concurrent:dist"
    "autoprefixer"
    "concat"
    "ngAnnotate"
    "copy:dist"
    "cdnify"
    "cssmin"
    "uglify"
    "filerev"
    "usemin"
    "replace:dist"
    "htmlmin"
  ]
  grunt.registerTask "default", [
    "newer:jshint"
    "test"
    "build"
  ]

  grunt.registerTask "install", [
    "wiredep"
    "injector"
  ]
